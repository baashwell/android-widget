package uk.ac.aber.bea6.sem2220.widgetAssignment;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;

public class AppWidgetConfigure extends Activity {
	int appWidgetId;
	String xmlUrl = "http://users.aber.ac.uk/aos/CSM22/conf1data.php";
	String xml = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setResult(RESULT_CANCELED);

		Context context = getApplicationContext();
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		XMLDecoder xmldec = new XMLDecoder();
		SharedPreferences pref = PreferenceManager
				.getDefaultSharedPreferences(context);

		xmldec.setXmlUrl(pref.getString("choose_conference", "One"));

		xmldec.setXmlString(xmldec.loadXMLDoc());

		List<Conference> conferences = null;

		if (extras != null) {
			appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
					AppWidgetManager.INVALID_APPWIDGET_ID);
		}

		RemoteViews views = new RemoteViews(context.getPackageName(),
				R.layout.appwidget);

		try {
			conferences = xmldec.decodeXmlString();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Conference conference = conferences.get(0);
		Session nextSession = conference.getNearestSession(conference
				.getCurrentTime());

		if (nextSession != null) {
			views.setTextViewText(R.id.nextTime, nextSession.getStartTime()
					.getHours()
					+ ":"
					+ nextSession.getStartTime().getMinutes()
					+ " - "
					+ nextSession.getEndTime().getHours()
					+ ":"
					+ nextSession.getEndTime().getMinutes());
			views.setTextViewText(R.id.nextType, nextSession.getType());
			views.setTextViewText(R.id.nextTitle, nextSession.getSessionTitle());

			Session secondSession = conference.getNearestSession(nextSession
					.getStartTime());

			if (secondSession != null) {
				views.setTextViewText(R.id.secondTime, secondSession
						.getStartTime().getHours()
						+ ":"
						+ getMinutes(secondSession.getStartTime().getMinutes())
						+ " - "
						+ secondSession.getEndTime().getHours()
						+ ":"
						+ getMinutes(secondSession.getEndTime().getMinutes()));
				views.setTextViewText(R.id.secondType, secondSession.getType());
				views.setTextViewText(R.id.secondTitle,
						secondSession.getSessionTitle());
			}
		}
		views.setTextViewText(R.id.latestMessage,
				conference.getCurrentMessage());

		Intent chooseIntent = new Intent(context,
				ChooseConferenceActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
				chooseIntent, 0);
		views.setOnClickPendingIntent(R.id.chooseConference, pendingIntent);
		AppWidgetManager appWidgetManager = AppWidgetManager
				.getInstance(context);

		appWidgetManager.updateAppWidget(appWidgetId, views);

		Intent resultValue = new Intent();
		resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
		setResult(RESULT_OK, resultValue);
		finish();
	}

	/**
	 * fix 0 minutes issue
	 * 
	 * @param minutes
	 *            integer value of minutes
	 * @return string representation of minutes
	 */
	private String getMinutes(int minutes) {
		String returnMinutes = "00";

		if (minutes != 0) {
			returnMinutes = String.valueOf(minutes);
		}

		return returnMinutes;
	}

}
