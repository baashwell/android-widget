package uk.ac.aber.bea6.sem2220.widgetAssignment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import android.text.format.Time;

/**
 * @author Ben Ashwell
 *
 */
public class Conference {

	private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
	private Date currentTime;
	private String currentMessage;
	private List<Session> sessions;

	public Conference() {
		sessions = new ArrayList();
	}

	public Conference(String currentTime, String currentMessage)
			throws ParseException {
		this.currentTime = dateFormat.parse(currentTime);
		this.currentMessage = currentMessage;
		sessions = new ArrayList();
	}

	/**
	 * Add a session to the sessions list
	 * 
	 * @param startTime
	 * @param endTime
	 * @param sessionTitle
	 * @param Type
	 * @throws ParseException
	 */
	public void addSession(String startTime, String endTime,
			String sessionTitle, String Type) throws ParseException {
		sessions.add(new Session(startTime, endTime, sessionTitle, Type));
	}

	/**
	 * @return the currentTime
	 */
	public Date getCurrentTime() {
		return currentTime;
	}

	/**
	 * @param currentTime
	 *            the currentTime to set
	 * @throws ParseException
	 */
	public void setCurrentTime(String currentTime) throws ParseException {
		this.currentTime = dateFormat.parse(currentTime);
	}

	/**
	 * @return the currentMessage
	 */
	public String getCurrentMessage() {
		return currentMessage;
	}

	/**
	 * @param currentMessage
	 *            the currentMessage to set
	 */
	public void setCurrentMessage(String currentMessage) {
		this.currentMessage = currentMessage;
	}

	/**
	 * Get the nearest session to a given time
	 * 
	 * @param currentTime
	 * @return session that is closest
	 */
	public Session getNearestSession(Date currentTime) {
		Session returnSession = null;
		Collections.sort(sessions);

		for (Session session : sessions) {
			if (session.getStartTime().after(currentTime)) {
				returnSession = session;
				break;
			}
		}

		return returnSession;
	}

	/**
	 * Get all sessions
	 * 
	 * @return list of sessions
	 */
	public List<Session> getSessions() {
		return sessions;
	}

}
