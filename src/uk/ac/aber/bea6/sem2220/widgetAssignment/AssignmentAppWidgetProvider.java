package uk.ac.aber.bea6.sem2220.widgetAssignment;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import uk.ac.aber.bea6.sem2220.widgetAssignment.R;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.RemoteViews;

public class AssignmentAppWidgetProvider extends AppWidgetProvider {

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		final int N = appWidgetIds.length;

		// Perform this loop procedure for each App Widget that belongs to this
		// provider
		for (int i = 0; i < N; i++) {
			int appWidgetId = appWidgetIds[i];

			XMLDecoder xmldec = new XMLDecoder();
			SharedPreferences pref = PreferenceManager
					.getDefaultSharedPreferences(context);

			xmldec.setXmlUrl(pref.getString("choose_conference", "One"));
			xmldec.setXmlString(xmldec.loadXMLDoc());

			List<Conference> conferences = null;
			try {
				conferences = xmldec.decodeXmlString();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// Get the layout for the App Widget and attach an on-click listener
			// to the button
			RemoteViews views = new RemoteViews(context.getPackageName(),
					R.layout.appwidget);

			Conference conference = conferences.get(0);
			Session nextSession = conference.getNearestSession(conference
					.getCurrentTime());

			if (nextSession != null) {
				views.setTextViewText(R.id.nextTime, nextSession.getStartTime()
						.getHours()
						+ ":"
						+ getMinutes(nextSession.getStartTime().getMinutes())
						+ " - "
						+ nextSession.getEndTime().getHours()
						+ ":"
						+ getMinutes(nextSession.getEndTime().getMinutes()));
				views.setTextViewText(R.id.nextType, nextSession.getType());
				views.setTextViewText(R.id.nextTitle,
						nextSession.getSessionTitle());

				Session secondSession = conference
						.getNearestSession(nextSession.getStartTime());

				if (secondSession != null) {
					views.setTextViewText(R.id.secondTime, secondSession
							.getStartTime().getHours()
							+ ":"
							+ getMinutes(secondSession.getStartTime()
									.getMinutes())
							+ " - "
							+ secondSession.getEndTime().getHours()
							+ ":"
							+ getMinutes(secondSession.getEndTime()
									.getMinutes()));
					views.setTextViewText(R.id.secondType,
							secondSession.getType());
					views.setTextViewText(R.id.secondTitle,
							secondSession.getSessionTitle());
				}
			}
			views.setTextViewText(R.id.latestMessage,
					conference.getCurrentMessage());

			Intent chooseIntent = new Intent(context,
					ChooseConferenceActivity.class);
			PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
					chooseIntent, 0);
			views.setOnClickPendingIntent(R.id.chooseConference, pendingIntent);
			// Tell the AppWidgetManager to perform an update on the current app
			// widget
			appWidgetManager.updateAppWidget(appWidgetId, views);
		}
	}

	/**
	 * fix 0 minutes issue
	 * 
	 * @param minutes
	 *            integer value of minutes
	 * @return string representation of minutes
	 */
	private String getMinutes(int minutes) {
		String returnMinutes = "00";

		if (minutes != 0) {
			returnMinutes = String.valueOf(minutes);
		}

		return returnMinutes;
	}

}
