/**
 * 
 */
package uk.ac.aber.bea6.sem2220.widgetAssignment;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.os.StrictMode;
import android.text.format.Time;
import android.util.Log;

/**
 * @author Ben Ashwell
 *
 */
public class XMLDecoder {

	private String xmlString = "";
	private Document DOM;
	String xmlUrl = "";

	public XMLDecoder() {

	}

	/**
	 * Set xml String
	 * 
	 * @param xml
	 */
	public void setXmlString(String xml) {
		xmlString = xml;
	}

	/**
	 * Decode the xml string
	 * 
	 * @return list of conferences
	 * @throws ParseException
	 */
	public List<Conference> decodeXmlString() throws ParseException {
		DOM = getDom();
		List<Conference> conferences = new ArrayList<Conference>();

		NodeList conferenceList = DOM.getElementsByTagName("conference");
		for (int i = 0; i < conferenceList.getLength(); i++) {
			Conference conference = new Conference();
			NodeList conferenceChildList = conferenceList.item(i)
					.getChildNodes();

			for (int j = 0; j < conferenceChildList.getLength(); j++) {

				if (conferenceChildList.item(j).getNodeName().toString()
						.equals("currentTime")) {
					try {
						conference.setCurrentTime(conferenceChildList.item(j)
								.getTextContent().toString());
					} catch (DOMException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				if (conferenceChildList.item(j).getNodeName().toString()
						.equals("currentMessage")) {
					conference.setCurrentMessage(conferenceChildList.item(j)
							.getTextContent());
				}
				if (conferenceChildList.item(j).getNodeName().toString()
						.equals("session")) {
					NamedNodeMap attributes = conferenceChildList.item(j)
							.getAttributes();
					NodeList sessionInfoList = conferenceChildList.item(j)
							.getChildNodes();
					String startTime = attributes.getNamedItem("startTime")
							.getTextContent();
					;
					String endTime = attributes.getNamedItem("endTime")
							.getTextContent();
					String sessionTitle = "";
					String type = "";

					for (int k = 0; k < sessionInfoList.getLength(); k++) {

						if (sessionInfoList.item(k).getNodeName().toString()
								.equals("sessiontitle")) {
							sessionTitle = sessionInfoList.item(k)
									.getTextContent();
						}
						if (sessionInfoList.item(k).getNodeName().toString()
								.equals("type")) {
							type = sessionInfoList.item(k).getTextContent();
						}
					}

					conference.addSession(startTime, endTime, sessionTitle,
							type);
				}
				conferences.add(conference);
			}
		}
		return conferences;
	}

	/**
	 * Get the DOM for the xml string
	 * 
	 * @return DOM representation of XML
	 */
	private Document getDom() {
		Document returnDom = null;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource input = new InputSource();

			input.setCharacterStream(new StringReader(xmlString));
			returnDom = db.parse(input);

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return returnDom;
	}

	/**
	 * Load in the xml doc from a URL
	 * 
	 * @return XML String
	 */
	public String loadXMLDoc() {
		URL url;
		String xml = "";
		try {
			url = new URL(xmlUrl);

			// this stuff used to allow connection. PermitAll is a problem but
			// need to get something working
			StrictMode.ThreadPolicy threadPolicy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(threadPolicy);

			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost post = new HttpPost(xmlUrl);
			HttpResponse response = httpClient.execute(post);
			HttpEntity httpEntity = response.getEntity();
			xml = EntityUtils.toString(httpEntity);

			// HttpURLConnection connection = (HttpURLConnection) url
			// .openConnection();
			// InputStream inputStream = connection.getInputStream();
			// byte[] buffer = new byte[1024];
			// String xmlInput = "";
			//
			// while (inputStream.read(buffer) > 0) {
			// xmlInput += buffer;
			// }

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return xml;
	}

	/**
	 * @return the xmlUrl
	 */
	public String getXmlUrl() {
		return xmlUrl;
	}

	/**
	 * @param xmlUrl
	 *            the xmlUrl to set
	 */
	public void setXmlUrl(String choice) {
		String newUrl = "http://users.aber.ac.uk/aos/CSM22/";
		System.out.println(choice);
		if (choice.equals("Two")) {
			newUrl += "conf2data.php";
		} else {
			newUrl += "conf1data.php";
		}
		System.out.println(newUrl);
		this.xmlUrl = newUrl;
	}

}
