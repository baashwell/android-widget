package uk.ac.aber.bea6.sem2220.widgetAssignment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.text.format.Time;

/**
 * @author Ben Ashwell
 *
 */
public class Session implements Comparable {

	private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
	private Date startTime;
	private Date endTime;
	private String sessionTitle;
	private String type;

	public Session(String startTime, String endTime, String sessionTitle,
			String type) throws ParseException {
		this.startTime = dateFormat.parse(startTime);
		this.endTime = dateFormat.parse(endTime);
		this.sessionTitle = sessionTitle;
		this.type = type;
	}

	/**
	 * @return the startTime
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 * @throws ParseException
	 */
	public void setStartTime(String startTime) throws ParseException {
		this.startTime = dateFormat.parse(startTime);
	}

	/**
	 * @return the endTime
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime
	 *            the endTime to set
	 * @throws ParseException
	 */
	public void setEndTime(String endTime) throws ParseException {
		this.endTime = dateFormat.parse(endTime);
	}

	/**
	 * @return the sessionTitle
	 */
	public String getSessionTitle() {
		return sessionTitle;
	}

	/**
	 * @param sessionTitle
	 *            the sessionTitle to set
	 */
	public void setSessionTitle(String sessionTitle) {
		this.sessionTitle = sessionTitle;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	@Override
	public int compareTo(Object another) {
		int returnInt = 0;
		Session anotherSession = (Session) another;

		if (this.startTime.before(anotherSession.startTime)) {
			returnInt = -1;
		}
		if (this.startTime.after(anotherSession.startTime)) {
			returnInt = 1;
		}

		return returnInt;
	}

}
